# Abandoned #
This project is dead in favour of [eggdrop-rpc](https://bitbucket.org/wilhelmy/eggdrop-rpc)
It proved as too difficult to embed libmicrohttpd into eggdrop because eggdrop is not threadsafe and libmicrohttpd is hard to manage in singlethreaded mode. I'm keeping the code mostly for historical reasons.