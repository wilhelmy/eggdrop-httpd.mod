/*
 * httpd.c -- part of httpd.mod
 *
 *   Hypertext transfer protocol daemon that runs as part of eggdrop.
 *   Allows scripts and modules to expose functionality over the web.
 *
 * $Id$
 */

/*
 * Copyright (c) 2014-2015 Moritz Wilhelmy
 *
 * This file is part of eggdrop-httpd.mod.
 *
 * eggdrop-httpd.mod is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * eggdrop-httpd.mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * eggdrop-httpd.mod.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Project goal
 *  ultimately I want this to replace the httpd that comes with stats.mod
 */

#define MODULE_NAME "httpd"
#define MAKING_HTTPD

#define MAX_BINDS 42 /* maximum bound URLs in httpd */

#include "src/mod/module.h"
#include "src/mod/httpd.mod/httpd.h"
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <unistd.h>
#include <semaphore.h>

#include <microhttpd.h>

#undef global
static Function *global = NULL; /* eggdrop's global function table */

static size_t usedmem = 0; /* modified whenever memory is allocated or freed */
struct settings {
	int httpd_port;
	char auth_user[20];
	char auth_pass[20];
};

static struct settings settings = {
	/* FIXME This is C99 syntax. Clarify which century we live in to check
	 * if this is really okay */

	.httpd_port = 8080,
	.auth_user = "\0",
	.auth_pass = "\0",
};

static unsigned long requests = 0; /* number of requests handled */
static struct MHD_Daemon *httpd = NULL;
static int pipefd[2] = {-1,-1};
static Tcl_Channel http_ready_chan = NULL;
static struct Bind binds[MAX_BINDS];
static unsigned long nbind = 0; /* index behind last allocated bind */

/* since http requests are processed sequentially for now:
 * http_event reads here, handle_http writes here.
 * To prevent any other microhttpd threads from accidentally overwriting this,
 * it's protected by a semaphore.
 * The semaphore is locked as follows:
 *  - initialized to 0
 *  - all microhttpd threads signal the pipe, then calls sem_wait()
 *  - the eggdrop main thread wakes up due to the pipe being signaled
 *    and calls sem_post when done.
 */
static sem_t sem;
struct Req *cur_req = NULL;

static int httpd_expmem()
{
	return usedmem;
}

static void httpd_report(int idx, int details)
{
	if (details) {
		dprintf(idx, "    Using %zu byte%s of memory\n", usedmem,
			(usedmem != 1) ? "s" : "");
		dprintf(idx, "    %lu requests handled.\n", requests);
	}
}

static tcl_strings mystrings[] = {
	{"httpd-auth-user", settings.auth_user, sizeof(settings.auth_user),  0},
	{"httpd-auth-pass", settings.auth_pass, sizeof(settings.auth_pass),  0},
	{NULL,              NULL,               0,                           0}
};

static tcl_ints myints[] = {
	{"httpd-port",    &settings.httpd_port, 0},
	{NULL,            NULL,                 0}
};

static Function httpd_table[] = {
	/* 0 - 3 */
	(Function) httpd_start,
	(Function) httpd_close,
	(Function) httpd_expmem,
	(Function) httpd_report,
	/* 4 - 7 */
	(Function) httpd_bind,
};

#include "misc.c"
#include "mthread.c"

static int notification_from_httpd(void)
{
	char buf[1];

	if (read(pipefd[0], buf, 1) == -1 &&
		(errno == EAGAIN || errno == EWOULDBLOCK))
		return 0;
	return 1;
}

static void http_event(ClientData clientData, int mask)
{
	Context;

	/* TODO what about writable and exception? */
	if (!(mask & TCL_READABLE))
		return;

	if (!notification_from_httpd())
		return;
	
	chatout("poop.\n");

	sem_post(&sem); /* unlock microhttpd thread */
}

static char *httpd_close()
{
	Context;
	rem_tcl_strings(mystrings);
	rem_tcl_ints(myints);

	if (httpd)
		MHD_stop_daemon(httpd);

	if (http_ready_chan) {
		Tcl_DeleteChannelHandler(http_ready_chan, http_event,
			(ClientData)(intptr_t)pipefd[0]);
		Tcl_UnregisterChannel(interp, http_ready_chan);
		http_ready_chan = NULL;
	}

	if (pipefd[0] != -1)
		close(pipefd[0]);
	if (pipefd[1] != -1)
		close(pipefd[1]);

	sem_destroy(&sem);

	module_undepend(MODULE_NAME);
	return NULL;
}

static int httpd_bind(char const *method, char const *wildcard, httpd_cb callback, int eggthread)
{
	char *wild, *meth;
	if (nbind >= MAX_BINDS) {
		putlog(LOG_MISC, "*", "(!) Please recompile httpd.mod with a "
			"higher value for MAX_BINDS");
		return 0;
	}

	wild = binds[nbind].wildcard;
	meth = binds[nbind].method;

	strncpyz(wild, wildcard, HTTPD_WILDLEN);

	/* if method is NULL, all methods are bound */
	if (method)
		strncpyz(meth, method, HTTPD_METHODLEN);
	else
		meth[0] = '\0';

	binds[nbind].func = callback;
	binds[nbind].eggthread = eggthread;

	nbind++;

	return 1;
}

char *httpd_start(Function *global_funcs)
{
	int flags;

	global = global_funcs;

	Context;

	module_register(MODULE_NAME, httpd_table, 0, 0);

	if (!module_depend(MODULE_NAME, "eggdrop", 106, 0)
	 && !module_depend(MODULE_NAME, "eggdrop", 108, 0)) {
		module_undepend(MODULE_NAME);
		return "This module requires Eggdrop 1.6.0 or later.";
	}

	add_tcl_ints(myints);
	add_tcl_strings(mystrings);

	bzero(binds, sizeof(binds));

	/* start singlethreaded httpd. The thread will be responsible for
	 * invoking its own select()-based event loop; as soon as select
	 * returns, it will invoke the callback (handle_http) which will pass
	 * all its parameters back to the eggdrop thread by assigning the global
	 * request struct pointer and then signaling Tcl's event handler and
	 * waiting on the semaphore. The eggdrop/Tcl thread will then handle the
	 * request and signal the httpd thread to resume by posting on the
	 * semaphore. Afterwards, both threads continue their normal operation.
	 *
	 * This mode of operation was chosen because it's apparently rather
	 * painful (impossible?) to get the file descriptor list from
	 * microhttpd in a non fd_set format, which would be hard to register in
	 * Tcl, and Tcl not allowing using an interp from a different thread
	 * than it was created in. It also means that the http request handlers
	 * need to run as fast as possible */

	httpd = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY,
		settings.httpd_port,
		NULL, NULL, /* <- access checks */
		handle_http, NULL, /* <- callback */
   
		MHD_OPTION_END);

	if (!httpd) {
		httpd_close();
		return "Error launching httpd. Module not loaded.";
	}

	if (pipe(pipefd) == -1) {
		httpd_close();
		return "Error creating httpd pipe. Module not loaded.";
	}

	/* try to set the pipe nonblocking. If getting the flags fails, proceed
	 * anyway. */
	if ((flags = fcntl(pipefd[0], F_GETFL, 0)) == -1)
		flags = 0;
	fcntl(pipefd[0], F_SETFL, flags | O_NONBLOCK);

	http_ready_chan = Tcl_MakeFileChannel((ClientData)(intptr_t)pipefd[0], TCL_READABLE);
	if (http_ready_chan == NULL) {
		httpd_close();
		return "Error making httpd channel. Module not loaded.";
	}

	Tcl_RegisterChannel(interp, http_ready_chan);
	Tcl_CreateChannelHandler(http_ready_chan, TCL_READABLE,
		http_event, (ClientData)(intptr_t)pipefd[0]);

	sem_init(&sem, 0, 0);

	init_binds();

	return NULL;
}
