/*
 * mthread.c -- part of httpd.mod
 * 
 *   This file is supposed to contain only the code that runs inside the
 *   microhttpd thread(s). The httpd.c is only supposed to only contain code
 *   that runs in the eggdrop main thread. I hope this can help avoid some
 *   confusion.
 *
 * $Id$
 */

/*
 * Copyright (c) 2014-2015 Moritz Wilhelmy
 *
 * This file is part of eggdrop-httpd.mod.
 *
 * eggdrop-httpd.mod is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * eggdrop-httpd.mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * eggdrop-httpd.mod.  If not, see <http://www.gnu.org/licenses/>.
 */

static inline void notify_eggdrop_thread(void)
{
	write(pipefd[1], " ", 1); /* notify main eggdrop thread */
}

static inline void wait_egg(void)
{
	notify_eggdrop_thread();

	/* wait for the eggdrop thread to finish processing the request */
	while (sem_wait(&sem) == -1 && errno == EINTR);
}

static int handle_http (void *cls, struct MHD_Connection *connection,
	const char *url, const char *method, const char *version,
	const char *upload_data, size_t *upload_data_size, void **con_cls)
{
	struct Req req;
	int i;

	req.cls              = cls;
	req.connection       = connection;
	req.url              = url;
	req.method           = method;
	req.version          = version;
	req.upload_data      = upload_data;
	req.upload_data_size = upload_data_size;
	req.con_cls          = *con_cls;
	req.ret              = 0;

	/* search first matching bind and trigger it */
	for (i = 0; i < MAX_BINDS && i < nbind && binds[i].wildcard[0]; i++) {
		if ((!method[0] || !strcmp(method, binds[i].method)) &&
		wild_match(binds[i].wildcard, url)) {
			req.bind = &binds[i];
			/* decide whether to call into Tcl or not. The idea is
			 * that only code that absolutely *needs* to be run in
			 * the eggdrop thread (e.g. because Tcl is involved)
			 * needs to call across threads. */
			if (binds[i].eggthread) {
				cur_req = &req;
				wait_egg();
			} else {
				binds[i].func(&req);
			}
			goto Out;
		}
	}

	/* no bind triggered. 404 */
	return_404(&req, NULL);

Out:
	*con_cls = req.con_cls;
	requests++;

	return req.ret;
}
