/*
 * misc.c -- part of httpd.mod
 * 
 *   This file contains ready-made http binds.
 *
 * $Id$
 */
/*
 * Copyright (c) 2014-2015 Moritz Wilhelmy
 *
 * This file is part of eggdrop-httpd.mod.
 *
 * eggdrop-httpd.mod is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * eggdrop-httpd.mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * eggdrop-httpd.mod.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef free /* eggdrop redefines this, but microhttpd allocates stuff with malloc */
#undef free
#endif

#define h1(x,...) "<!DOCTYPE html>\n<html>\n"\
	"<head>\n<title>" x "</title></head>\n"\
	"<body>\n<h1>" x "</h1>\n</body>\n" __VA_ARGS__ \
	"</html>\n"

/* helper function to be called internally to authenticate clients on
 * "critical" API calls against a static password in the config.
 * i.e. all internal callbacks should start with
 *     if (authenticate(req, "...")) return;
 * so that other people on the host who don't know the password can't take over
 * the bot.
 *
 * Returns:
 *  0 = authentication was attempted and successful
 *  1 = invalid user/password
 *  2 = no basic authentication attempted
 *
 * The message "Authorization required" will have been dispatched already in
 * both non-success cases.
 */
static int authenticate_static(struct Req *req, char const *realm)
{
	int status = 2;
	char *user, *pass;

	if (settings.auth_user[0] == '\0') /* authentication not configured */
		return 0;

	user = MHD_basic_auth_get_username_password(req->connection, &pass);

	if (user != NULL)
		status = !(strcmp(user, settings.auth_user) == 0 &&
		           strcmp(pass, settings.auth_pass) == 0);

	/* XXX free'ing these segfaulted at some point.
	 * No longer happens with -O0. Valgrind reports nothing. Check this. */
	if (user) free(user);
	if (pass) free(pass);

	if (status) {
		struct MHD_Response *r;
		char const *page = h1("Authorization required");

		r = MHD_create_response_from_buffer(strlen(page), (void *)page,
				MHD_RESPMEM_PERSISTENT);
		req->ret = MHD_queue_basic_auth_fail_response(req->connection,
				realm, r);
	}

	return status;
}

static void return_404(struct Req *req, char const *page)
{
	char const *msg404 = h1("404 Not found");
	struct MHD_Response *response;

	if (!page) page = msg404;

	response = MHD_create_response_from_buffer(strlen(page),
			(void*)page, MHD_RESPMEM_PERSISTENT);

	if (response) {
		MHD_add_response_header(response, "Content-Type", "text/html");
		req->ret = MHD_queue_response(req->connection, MHD_HTTP_NOT_FOUND, response);
		MHD_destroy_response(response);
	} else {
		req->ret = MHD_NO;
	}
}

static void hi(struct Req *req)
{
	return_404(req, "Hi");
}

static void yo(struct Req *req)
{
	int auth = authenticate_static(req, "fort kickass");

	debug("Debug: url was %s %d", req->url, auth);

	if (auth) return;

	return_404(req, "Yo");
}

static void init_binds(void)
{
	httpd_bind("GET", "/", hi, 0);
	httpd_bind("GET", "/auth", yo, 0);
}
