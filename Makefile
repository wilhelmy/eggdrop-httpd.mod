# Makefile for src/mod/httpd.mod/
# $Id$

srcdir = .
EXTRA_FLAGS= -fPIC -pthread -Wall
EXTRA_LIBS= -lmicrohttpd


doofus:
	@echo ""
	@echo "Let's try this from the right directory..."
	@echo ""
	@cd ../../../ && make modules
#more doofus
install:
	@cd ../../../ && make install

static: ../httpd.o

modules: ../../../httpd.$(MOD_EXT)

../httpd.o: $(wildcard *.c *.h)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(EXTRA_FLAGS) -DMAKING_MODS -c $(srcdir)/httpd.c
	@rm -f ../httpd.o
	mv httpd.o ../

../../../httpd.$(MOD_EXT): ../httpd.o
	$(LD) $(EXTRA_FLAGS) -o ../../../httpd.$(MOD_EXT) ../httpd.o $(XLIBS) $(MODULE_XLIBS) $(EXTRA_LIBS)
	$(STRIP) ../../../httpd.$(MOD_EXT)

depend:
	$(CC) $(CFLAGS) -MM $(srcdir)/httpd.c -MT ../httpd.o > .depend

clean:
	@rm -vf .depend *.o *.$(MOD_EXT) *~ ../httpd.o

distclean: clean

#safety hash
../httpd.o: httpd.c ../../../src/mod/module.h ../../../src/main.h \
 ../../../config.h ../../../lush.h ../../../src/lang.h \
 ../../../src/eggdrop.h ../../../src/flags.h ../../../src/cmdt.h \
 ../../../src/tclegg.h ../../../src/tclhash.h ../../../src/chan.h \
 ../../../src/users.h ../../../src/compat/compat.h \
 ../../../src/compat/inet_aton.h ../../../src/compat/snprintf.h \
 ../../../src/compat/memset.h ../../../src/compat/memcpy.h \
 ../../../src/compat/strcasecmp.h ../../../src/compat/strdup.h \
 ../../../src/compat/strftime.h ../../../src/mod/modvals.h \
 ../../../src/tandem.h
