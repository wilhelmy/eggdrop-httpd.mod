/*
 * httpd.h -- part of httpd.mod
 *
 *   This file is supposed to contain the C API for the module, for use within
 *   C extensions as well as the module itself.
 *
 * $Id$
 */
/*
 * Copyright (c) 2014-2015 Moritz Wilhelmy
 *
 * This file is part of eggdrop-httpd.mod.
 *
 * eggdrop-httpd.mod is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * eggdrop-httpd.mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * eggdrop-httpd.mod.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _HTTPD_H
#define _HTTPD_H

/* FIXME give things in here names that don't clash */
struct Req;
typedef void (*httpd_cb)(struct Req *);

/* this struct is used to pass data between the two threads */
struct Req {
	/* httpd -> eggdrop */
	void *cls;
	struct MHD_Connection *connection;
	const char *url;
	const char *method;
	const char *version;
	const char *upload_data;
	size_t *upload_data_size;
	void *con_cls;

	struct Bind *bind; /* bind that fired */

	/* eggdrop -> httpd */
	int ret;
};

#define HTTPD_WILDLEN 80
#define HTTPD_METHODLEN 8 /* 'DELETE' + 1 spare */
struct Bind {
	char wildcard[HTTPD_WILDLEN];
	char method[HTTPD_METHODLEN];
	httpd_cb func;
	int eggthread; /* run this bind in the Tcl thread or not */
};


#ifdef MAKING_HTTPD

EXPORT_SCOPE char *httpd_start();
static char *httpd_close();
static int httpd_expmem();
static void httpd_report();
static Tcl_ChannelProc http_event;

static int httpd_bind(char const *method, char const *wildcard, httpd_cb callback, int eggthread);


/* shamelessly use C99 features internally */
#define debug(...) putlog(LOG_DEBUG,"*",__VA_ARGS__)

#else /* public interface goes here */

#define httpd_bind ((int (*)(char const *, char const *, httpd_cb, int))httpd_funcs[4])

#endif /* MAKING_HTTPD */

#endif /* _HTTPD_H */
